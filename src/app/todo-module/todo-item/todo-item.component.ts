import { Component, Input, OnInit } from '@angular/core';
import { ToDoItemModel } from '../models/todo-item-model';
import { TodoService } from '../services/todo.service';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})

export class TodoItemComponent implements OnInit {
  @Input() todoItem: ToDoItemModel;

  constructor(private _todoService: TodoService) { }

  ngOnInit() {}

  deleteTodoItem(): ToDoItemModel[] {
    return this._todoService.deleteTodo(this.todoItem);
  }
}
