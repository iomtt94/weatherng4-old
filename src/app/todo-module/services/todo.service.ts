import { ToDoItemModel } from '../models/todo-item-model';
import { Injectable } from '@angular/core';

@Injectable()
export class TodoService {

  public todosList: ToDoItemModel[] = [];

  constructor() { }

  addTodo(userInput: string): number {
    const todo = new ToDoItemModel(userInput, false, Date.now());
    return this.todosList.unshift(todo);
  }

  deleteTodo(todo: ToDoItemModel): ToDoItemModel[] {
    if (this.todosList.indexOf(todo) !== -1) {
      return this.todosList.splice(this.todosList.indexOf(todo), 1);
    }
  }
}
