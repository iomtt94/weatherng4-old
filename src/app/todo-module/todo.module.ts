import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoService } from './services/todo.service';
import { NgModule } from '@angular/core';
import { MatInputModule, MdButtonModule } from '@angular/material';
import { TodoItemComponent } from './todo-item/todo-item.component';
import { InputAreaComponent } from './todo-input-area/input-area.component';
import { MatCheckboxModule } from '@angular/material';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

const routes: Routes = [
  { path: 'todoList', component: TodoListComponent },
];

@NgModule({
  declarations: [
    TodoItemComponent,
    InputAreaComponent,
    TodoListComponent,
  ],
  imports: [
    MatCheckboxModule,
    BrowserModule,
    MatInputModule,
    MdButtonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  providers: [TodoService],
  exports: []
})

export class TodoModule {}
