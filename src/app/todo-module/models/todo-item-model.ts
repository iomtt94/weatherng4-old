import { ITodo } from '../interfaces/ITodo.component';

export class ToDoItemModel implements ITodo {

  constructor(public title: string,
    public isComplited: boolean = false, public id: number) {
  }

  public toggleIsCompleted(): boolean {
    this.isComplited = !this.isComplited;
    return this.isComplited;
  }
}
