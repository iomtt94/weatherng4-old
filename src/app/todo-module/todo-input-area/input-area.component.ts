import { Component, OnInit, Input } from '@angular/core';
import { TodoService } from '../services/todo.service';
import { ToDoItemModel } from '../models/todo-item-model';

@Component({
  selector: 'app-input-area',
  templateUrl: './input-area.component.html',
  styleUrls: ['./input-area.component.css']
})

export class InputAreaComponent implements OnInit {
  @Input() userInput = '';

  constructor(private _todoService: TodoService) { }

  ngOnInit() { }

  addTodo(userInput: string): ToDoItemModel {
    if (this.userInput.trim() === '') return;
    this._todoService.addTodo(userInput);
    this.userInput = '';
  }
}
