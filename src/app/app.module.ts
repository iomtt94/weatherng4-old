import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { TodoModule } from './todo-module/todo.module';
import { WeatherModule } from './weather-module/weather.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AboutAuthorComponent } from './about-author/about-author.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SharedModule } from './shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';

const routes: Routes = [
  { path: '', redirectTo: 'todoList', pathMatch: 'full' },
  { path: 'author', component: AboutAuthorComponent },
  { path: '**', component: PageNotFoundComponent, pathMatch: 'full' },
];

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    AboutAuthorComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    TodoModule,
    WeatherModule,
    SharedModule,
    FormsModule,
    RouterModule.forRoot(routes),
  ],
  exports: [],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {}
