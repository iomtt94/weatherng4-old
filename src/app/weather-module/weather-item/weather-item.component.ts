import { IWeather } from './../Interfaces/IWeather';
import { Component, OnInit } from '@angular/core';
import { DarkSkySearchService } from '../services/darksky.service';

@Component({
  selector: 'app-weather-item',
  templateUrl: './weather-item.component.html',
  styleUrls: ['./weather-item.component.css']
})
export class WeatherItemComponent implements OnInit {

  constructor(private _darkSky: DarkSkySearchService) { }

  public currentDate = new Date();
  public weather: IWeather;
  public cityName: string = null;
  public summary: string;
  public iconType: string;
  public visibility: number;
  public windSpeed: number;
  public temparature: number;
  public cloudCover: number;
  public weatherType: string[] = ['clear-day', 'clear-night',
  'rain', 'snow', 'sleet', 'wind', 'fog', 'cloudy', 'partly-cloudy-day',
  'partly-cloudy-night', 'hail', 'thunderstorm', 'tornado'];

  ngOnInit() {
    this._darkSky.weatherSummary.debounceTime(200).subscribe((weather: IWeather) => {
      this.cityName = this._darkSky.cityName;
      this.weather = weather;
      this.summary = weather.currently.summary;
      this.visibility = weather.currently.visibility;
      this.windSpeed = weather.currently.windSpeed;
      this.temparature = weather.currently.temperature;
      this.cloudCover = weather.currently.cloudCover;
      this.iconType = weather.currently.icon;
    });

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this._darkSky.getWeather(position.coords.latitude, position.coords.longitude);
      });
    } else {
      alert('Geolocation is not supported by this browser.');
    }
  }
}
