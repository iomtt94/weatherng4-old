export interface ICityList {
  name: string;
  type: string;
  c: string;
  zmw: string;
  tz: string;
  tzs: string;
}
