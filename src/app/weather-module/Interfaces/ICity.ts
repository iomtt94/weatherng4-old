export interface ICity {
  c: string;
  l: string;
  lat: string;
  ll: string;
  lon: string;
  name: string;
  type: string;
  tz: string;
  tzs: string;
  zmw: string;
}
