import { ICityList } from '../Interfaces/ICityList';
import { Component, OnInit } from '@angular/core';
import { DarkSkySearchService } from '../services/darksky.service';
import { ICity } from '../Interfaces/ICity';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private _darkSky: DarkSkySearchService) {}

  public userSearchText = '';
  public citiesList: any[];
  public isValidSearch = false;

  ngOnInit() {}

  searchCity(): void {
    if (this.userSearchText.length < 2) {
      this.isValidSearch = false;
      return;
    }

    this._darkSky.searchCity(this.userSearchText).subscribe((foundCities: ICity[]) => {
       this.citiesList = foundCities['RESULTS'].filter((city: ICity) => city.type === 'city');

       this.isValidSearch = true;
    });
  }

  showWeather(city: ICity): void {
    const { lat: lattitude, lon: longtitude, name: cityName } = city;

    this._darkSky.getWeather(lattitude, longtitude, cityName);
  }

  validation(): boolean {
    if (this.userSearchText.trim() !== '') return;
    
    this.citiesList = [];
  }
}
