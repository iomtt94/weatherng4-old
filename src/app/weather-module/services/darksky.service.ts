import { IWeather } from './../Interfaces/IWeather';
import { Subject } from 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ICityList } from '../Interfaces/ICityList';

@Injectable()
export class DarkSkySearchService {

  constructor(private _http: HttpClient) { }

  public weatherSummary = new Subject<IWeather>();
  public cityName = '';

  getWeather(lat: string | number, long: string | number, cityName?: string): void {
    const weatherUrl = `https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/5c53fbb07fccd7fc7353591011a1544d/${lat},${long}`;
    this._http.get(weatherUrl).subscribe((weather: IWeather) => {
      this.cityName = cityName;
      return this.weatherSummary.next(weather);
    });
  }

  searchCity(userInput: string) {
    const autocompleteUrl = `http://autocomplete.wunderground.com/aq?query=${userInput}`;
    return this._http.jsonp(autocompleteUrl, 'cb').map((foundCities: ICityList[]) => {
      return foundCities;
    });
  }
}
